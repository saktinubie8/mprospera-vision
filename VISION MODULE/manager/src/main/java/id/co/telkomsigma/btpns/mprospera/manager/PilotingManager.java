package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.wisma.ScoringMmsPiloting;

public interface PilotingManager {

    ScoringMmsPiloting getActivePilotingOffice(String officeCode);

}