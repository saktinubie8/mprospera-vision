package id.co.telkomsigma.btpns.mprospera.manager;

import java.util.List;

import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;

public interface TerminalManager {

    Terminal getTerminalByImei(String imei);

    Terminal insertTerminal(Terminal terminal);

    Terminal updateTerminal(Terminal terminal);

    List<Terminal> getAllTerminal();

    void clearCache();

}