package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.UserDao;
import id.co.telkomsigma.btpns.mprospera.exception.UserProfileException;
import id.co.telkomsigma.btpns.mprospera.manager.UserManager;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by daniel on 3/31/15.
 */
@Service("userManager")
public class UserManagerImpl implements UserManager {

    @Autowired
    private UserDao userDao;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private static final Logger log = LoggerFactory.getLogger(UserManagerImpl.class);

    @Override
    @Cacheable(value = "vsn.user.userByUsername", unless = "#result == null")
    public User getUserByUsername(String username) {
        log.debug("in direct query getUserByUsername >> " + username);
        return userDao.findByUsername(username.toLowerCase());
    }

    @Override
    @Cacheable(value = "vsn.user.sessionKeyByUsername", unless = "#result == null")
    public String getSessionKeyByUsername(String username) {
        log.debug("in direct query getSessionKeyByUsername >> " + username);
        return userDao.findSessionKeyByUsername(username.toLowerCase());
    }

    @Override
    @Caching(put = {
            @CachePut(value = "vsn.user.userByUsername", key = "#user.username", unless = "#result == null"),
            @CachePut(value = "wln.user.userByUsername", key = "#user.username", unless = "#result == null"),
            @CachePut(value = "hwk.user.userByUsername", key = "#user.username", unless = "#result == null")}, evict = {
            @CacheEvict(value = "vsn.user.sessionKeyByUsername", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "vsn.user.getAllUser", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.user.userByLocId", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.user.getUserByUserId", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.user.findUserNonMS", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.user.userByLocId", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.user.userByUserId", allEntries = true, beforeInvocation = true)})
    public User insertUser(User user) throws UserProfileException {
        user.setUsername(user.getUsername().toLowerCase());
        user.setCreatedDate(new Date());
        user.setPassword("000000");
        user.setEnabled(true);
        user.setAccountNonLocked(true);
        user.setAccountNonExpired(true);
        user.setCredentialsNonExpired(true);
        // do search user by username / email address
        User temp0 = userDao.findByUsername(user.getUsername());
        User temp1 = userDao.findByEmailIgnoreCase(user.getEmail());
        if (temp0 != null && temp1 != null) {
            throw new UserProfileException(true, true);
        } else if (temp0 != null) {
            throw new UserProfileException(true, false);
        } else if (temp1 != null) {
            throw new UserProfileException(false, true);
        }
        user = userDao.save(user);
        return user;
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "vsn.user.userByUsername", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "vsn.user.sessionKeyByUsername", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "vsn.user.getAllUser", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.user.userByUsername", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.user.userByLocId", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.user.getUserByUserId", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.user.findUserNonMS", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.user.userByLocId", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.user.userByUsername", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.user.userByUserId", allEntries = true, beforeInvocation = true)})
    public User updateUser(User result) {
        User fromDb = userDao.findByUserId(result.getUserId());
        if (result.getPassword() != null && !result.getPassword().equalsIgnoreCase("")) {
            if (!fromDb.getPassword().equals(result.getPassword()))
                fromDb.setPassword(passwordEncoder.encode(result.getPassword()));
        }
        fromDb.setEmail(result.getEmail());
        fromDb.setName(result.getName());
        fromDb.setAddress(result.getAddress());
        fromDb.setCity(result.getCity());
        fromDb.setProvince(result.getProvince());
        fromDb.setPhone(result.getPhone());
        fromDb.setPhoneMobile(result.getPhoneMobile());
        fromDb.setOfficeCode(result.getOfficeCode());
        fromDb.setRoles(result.getRoles());
        fromDb.setRaw(result.getRaw());
        fromDb.setSessionKey(result.getSessionKey());
        fromDb.setSessionCreatedDate(result.getSessionCreatedDate());
        fromDb.setSessionTime(result.getSessionTime());
        fromDb.setDistrictCode(result.getDistrictCode());
        fromDb.setRoleUser(result.getRoleUser());
        fromDb.setRoleName(result.getRoleName());
        fromDb.setProsperaRoleId(result.getProsperaRoleId());
        fromDb.setLoginImei(result.getLoginImei());
        userDao.save(fromDb);
        return result;
    }

    @Override
    @CacheEvict(value = {"vsn.user.userByUsername", "vsn.user.sessionKeyByUsername",
            "vsn.user.getAllUser"}, allEntries = true, beforeInvocation = true)
    public void clearCache() {

    }

    @Override
    @Cacheable(value = "vsn.user.getAllUser", unless = "#result == null")
    public List<User> getAllUser() {
        // TODO Auto-generated method stub
        return userDao.findAll();
    }

    @Bean(name = "passwordEncoder")
    public PasswordEncoder getPasswordEncoder() {
        if (passwordEncoder == null) {
            passwordEncoder = new BCryptPasswordEncoder();
        }
        return passwordEncoder;
    }

}