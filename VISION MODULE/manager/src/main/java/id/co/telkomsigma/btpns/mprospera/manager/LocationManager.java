package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.location.Location;

public interface LocationManager {

    void clearCache();

    Location findByLocationId(String id);

}