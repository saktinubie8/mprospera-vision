package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.manageApk.ManageApk;

public interface ManageApkManager {

    ManageApk getByVersion(String version);

    Boolean isApkValid(String version);

    void clearCache();

}