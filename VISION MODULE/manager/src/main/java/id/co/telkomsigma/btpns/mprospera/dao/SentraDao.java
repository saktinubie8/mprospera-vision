package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SentraDao extends JpaRepository<Sentra, Long> {

    Sentra findByProsperaId(String prosperaId);

}