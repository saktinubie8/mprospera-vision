package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.exception.UserProfileException;
import id.co.telkomsigma.btpns.mprospera.model.user.User;

import java.util.List;

/**
 * Created by daniel on 3/31/15.
 */
public interface UserManager {

    /**
     * Gets single user by username
     *
     * @param username
     * @return user or null
     */
    User getUserByUsername(String username);

    /**
     * Gets single SessionKey by username
     *
     * @param username
     * @return user or null
     */
    String getSessionKeyByUsername(String username);

    /**
     * Saves new user (admin / CS)
     *
     * @param user
     * @return user or null
     */
    User insertUser(User user) throws UserProfileException;

    /**
     * Updates a user
     *
     * @param user
     * @return user or null
     */

    User updateUser(User user);

    void clearCache();

    List<User> getAllUser();

}