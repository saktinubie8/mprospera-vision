package id.co.telkomsigma.btpns.mprospera.service;

import java.util.List;

import id.co.telkomsigma.btpns.mprospera.exception.UserProfileException;
import id.co.telkomsigma.btpns.mprospera.manager.UserManager;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * Created by daniel on 3/31/15.
 */
@Service("userService")
public class UserService extends GenericService implements UserDetailsService {

    @Autowired
    private UserManager userManager;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
        User user = userManager.getUserByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("User '" + username + "' not found.");
        }
        return user;
    }

    public User updateUser(User user) {
        return userManager.updateUser(user);
    }

    public void addMobileUser(String username, String imei, String state, String officeCode) {
        User user = userManager.getUserByUsername(username);
        if (user == null) {
            User toAdd = new User();
            toAdd.setAccountNonExpired(true);
            toAdd.setAccountNonLocked(true);
            toAdd.setCredentialsNonExpired(true);
            toAdd.setEnabled(true);
            toAdd.setUsername(username);
            toAdd.setPassword("");
            toAdd.setName(username);
            toAdd.setCity(state);
            toAdd.setEmail(username + "@btpns.co.id");
            toAdd.setAddress("IMEI=" + imei);
            toAdd.setOfficeCode(officeCode);
            try {
                userManager.insertUser(toAdd);
            } catch (UserProfileException e) {
                // TODO Auto-generated catch block
                log.error(e.getMessage(), e);
            }
        }
    }

    public User logout(User user) {
        user.setSessionKey(null);
        user.setSessionTime(null);
        user.setLoginImei(null);
        userManager.updateUser(user);
        return user;
    }

    public User logoutScheduler(User user) {
        user.setSessionKey(null);
        user.setSessionTime(null);
        user.setLoginImei(null);
        userManager.updateUser(user);
        return user;
    }

    public List<User> getAllUser() {
        return userManager.getAllUser();
    }

}