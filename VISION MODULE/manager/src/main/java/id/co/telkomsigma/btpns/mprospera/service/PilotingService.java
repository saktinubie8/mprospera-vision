package id.co.telkomsigma.btpns.mprospera.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import id.co.telkomsigma.btpns.mprospera.manager.PilotingManager;
import id.co.telkomsigma.btpns.mprospera.model.wisma.ScoringMmsPiloting;

@Service("pilotingService")
public class PilotingService extends GenericService {

    @Autowired
    private PilotingManager pilotingMgr;

    public ScoringMmsPiloting getActivePilotingOffice(String officeCode) {
        return pilotingMgr.getActivePilotingOffice(officeCode);
    }

}