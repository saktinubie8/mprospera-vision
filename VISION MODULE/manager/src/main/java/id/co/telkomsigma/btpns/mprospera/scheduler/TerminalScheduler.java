package id.co.telkomsigma.btpns.mprospera.scheduler;

import java.util.List;

import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.service.TerminalService;
import id.co.telkomsigma.btpns.mprospera.service.UserService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@SuppressWarnings("ALL")
@Component
public class TerminalScheduler {

    @Autowired
    private TerminalService terminalService;

    @Autowired
    private UserService userService;

    protected final Log LOGGER = LogFactory.getLog(getClass());

    @Scheduled(cron = "0 0 0 * * ?")
    public void signOutAllTerminal() {
        List<Terminal> allTerminal = terminalService.getAllTerminal();
        List<User> allUser = userService.getAllUser();
        for (Terminal term : allTerminal) {
            LOGGER.trace("Try sign out terminal");
            terminalService.logoutScheduler(term);
            LOGGER.trace("Sign out terminal success");
        }
        for (User user : allUser) {
            LOGGER.trace("Try clear session");
            userService.logoutScheduler(user);
            LOGGER.trace("Clear session success");
        }
    }

}