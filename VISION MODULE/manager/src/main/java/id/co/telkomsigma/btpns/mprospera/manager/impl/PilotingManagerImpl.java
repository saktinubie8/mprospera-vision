package id.co.telkomsigma.btpns.mprospera.manager.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import id.co.telkomsigma.btpns.mprospera.dao.PilotingDao;
import id.co.telkomsigma.btpns.mprospera.manager.PilotingManager;
import id.co.telkomsigma.btpns.mprospera.model.wisma.ScoringMmsPiloting;

@Service("pilotingManager")
public class PilotingManagerImpl implements PilotingManager {

    @Autowired
    private PilotingDao pilotingDao;

    @Override
    @Cacheable(value = "vsn.wisma.pilot.getActivePilotingOffice", unless = "#result == null")
    public ScoringMmsPiloting getActivePilotingOffice(String officeCode) {
        return pilotingDao.findActivePilotingOffice(officeCode);
    }

}