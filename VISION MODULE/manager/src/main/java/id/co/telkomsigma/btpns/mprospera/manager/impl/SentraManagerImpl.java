package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;
import id.co.telkomsigma.btpns.mprospera.dao.SentraDao;
import id.co.telkomsigma.btpns.mprospera.manager.SentraManager;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;

@Service("sentraManager")
public class SentraManagerImpl implements SentraManager {

    @Autowired
    private SentraDao sentraDao;

    @Override
    @Caching(evict = {@CacheEvict(value = "vsn.sentra.findByProsperaId", key = "#sentra.prosperaId", beforeInvocation = true),
            @CacheEvict(value = "hwk.sentra.findBySentraId", key = "#sentra.sentraId", beforeInvocation = true),
            @CacheEvict(value = "hwk.sentra.getAllSentra", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.sentra.getAllSentraPageable", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.sentra.findSentraByCreatedDate", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.sentra.findSentraByCreatedDatePageable", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.sentra.isValidSentra", key = "#sentra.sentraId", beforeInvocation = true),
            @CacheEvict(value = "hwk.sentra.getSentraByGroupId", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.sentra.findAllSentraList", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.sentra.getSentraByRrn", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.sentra.findDeletedSentraList", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "irn.sentra.findBySentraId", key = "#sentra.sentraId", beforeInvocation = true),
            @CacheEvict(value = "irn.sentra.findByProsperaId", key = "#sentra.prosperaId", beforeInvocation = true),
            @CacheEvict(value = "irn.sentra.getSentraByWismaId", key = "#sentra.locationId", beforeInvocation = true),
            @CacheEvict(value = "wln.sentra.findByCreatedDate", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sentra.findByCreatedDatePageable", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sentra.findByRrn", key = "#sentra.rrn", beforeInvocation = true),
            @CacheEvict(value = "wln.sentra.findAll", allEntries = true, beforeInvocation = true)})
    public void save(Sentra sentra) {
        sentraDao.save(sentra);
    }

    @Override
    @CacheEvict(value = {"vsn.sentra.findByProsperaId"}, allEntries = true, beforeInvocation = true)
    public void clearCache() {
        // TODO Auto-generated method stub
    }

    @Override
    @Cacheable(value = "vsn.sentra.findByProsperaId", unless = "#result == null")
    public Sentra findByProsperaId(String prosperaId) {
        return sentraDao.findByProsperaId(prosperaId);
    }

    @Caching(evict = {@CacheEvict(value = "vsn.sentra.findByProsperaId", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.sentra.findBySentraId", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.sentra.getAllSentra", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.sentra.getAllSentraPageable", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.sentra.findSentraByCreatedDate", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.sentra.findSentraByCreatedDatePageable", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.sentra.isValidSentra", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.sentra.getSentraByGroupId", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.sentra.findAllSentraList", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.sentra.getSentraByRrn", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.sentra.findDeletedSentraList", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "irn.sentra.findBySentraId", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "irn.sentra.findByProsperaId", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "irn.sentra.getSentraByWismaId", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sentra.findByCreatedDate", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sentra.findByCreatedDatePageable", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sentra.findByRrn", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sentra.findAll", allEntries = true, beforeInvocation = true)})
    public void save(List sentra) {
        // TODO Auto-generated method stub
        //noinspection unchecked
        sentraDao.save(sentra);
    }

}