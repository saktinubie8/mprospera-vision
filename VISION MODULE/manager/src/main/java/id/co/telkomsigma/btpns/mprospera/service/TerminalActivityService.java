package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.TerminalActivityManager;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by Dzulfiqar on 11/12/15.
 */
@SuppressWarnings("ALL")
@Service("terminalActivityService")
public class TerminalActivityService extends GenericService {

    @Autowired
    private TerminalActivityManager terminalActivityManager;

    @Autowired
    private TerminalService terminalService;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    public TerminalActivity insertTerminalActivity(final TerminalActivity terminalActivity) {
        if (terminalActivity.getTerminalActivityId() == null)
            terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
        threadPoolTaskExecutor.execute(new Runnable() {

            @Override
            public void run() {
                terminalActivityManager.insertTerminalActivity(terminalActivity);
            }

        });
        return terminalActivity;
    }

    public void saveTerminalActivityAndMessageLogs(final TerminalActivity terminalActivity, final List<MessageLogs> messageLogs) {
        if (terminalActivity.getTerminalActivityId() == null)
            terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
        terminalActivityManager.insertTerminalActivity(terminalActivity);
    }

    public void submitTerminalData(String transmissionDateAndTime, String retrievalReferenceNumber, String user,
                                   String sessionKey, String imei, String processor, String ram, String gpsVersion, String totalDeviceMemory,
                                   String freeDeviceMemory, String operatorName, String signal, String apkVersion) {
        Terminal terminal = terminalService.loadTerminalByImei(imei);
        terminal.setProcessor(processor);
        terminal.setRam(ram);
        terminal.setGpsVersion(gpsVersion);
        terminal.setTotalDeviceMemory(totalDeviceMemory);
        terminal.setFreeDeviceMemory(freeDeviceMemory);
        terminal.setApplicationVersion(apkVersion);
        terminalService.updateTerminal(terminal);
        String rawTerminalData = "transmissionDateAndTime=" + transmissionDateAndTime + ",retrievalReferenceNumber="
                + retrievalReferenceNumber + ",username=" + user + ",apkVersion=" + apkVersion + ",sessionKey="
                + sessionKey + ",imei=" + imei + ",processor=" + processor + ",ram=" + ram + ",gpsVersion=" + gpsVersion
                + ",totalDeviceMemory=" + totalDeviceMemory + ",freeDeviceMemory=" + freeDeviceMemory + ",operatorName="
                + operatorName + ",signal=" + signal;
        TerminalActivity terminalActivity = new TerminalActivity();
        terminalActivity.setCreatedDate(new Date());
        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SUBMIT_ANDROID_DATA);
        terminalActivity.setReffNo(retrievalReferenceNumber);
        terminalActivity.setTerminal(terminal);
        terminalActivity.setUsername(user);
        insertTerminalActivity(terminalActivity);
    }

}