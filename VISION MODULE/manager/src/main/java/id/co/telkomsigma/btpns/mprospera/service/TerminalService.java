package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.TerminalManager;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Dzulfiqar on 11/11/15.
 */
@Service("terminalService")
public class TerminalService extends GenericService {

    @Autowired
    private TerminalManager terminalManager;

    public Terminal createTerminal(Terminal terminal) {
        return terminalManager.insertTerminal(terminal);
    }

    public Terminal loadTerminalByImei(final String imei) throws UsernameNotFoundException {
        return terminalManager.getTerminalByImei(imei);
    }

    public Terminal updateTerminal(final Terminal terminal) {
        terminalManager.updateTerminal(terminal);
        return terminal;
    }

    public Terminal logout(Terminal terminal) {
        terminal.setLogin(false);
        terminalManager.updateTerminal(terminal);
        return terminal;
    }

    public Terminal logoutScheduler(Terminal terminal) {
        terminal.setLogin(false);
        terminalManager.updateTerminal(terminal);
        return terminal;
    }

    public List<Terminal> getAllTerminal() {
        return terminalManager.getAllTerminal();
    }

}