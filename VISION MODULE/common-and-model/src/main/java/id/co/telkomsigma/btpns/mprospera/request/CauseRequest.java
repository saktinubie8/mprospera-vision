package id.co.telkomsigma.btpns.mprospera.request;

import java.util.List;

/**
 * Created by Dzulfiqar on 6/05/2017.
 */
public class CauseRequest {

    private CauseRequest cause;
    private List<StackTraceRequest> stackTrace;
    private List<SuppressedException> suppressedExceptions;
    private String detailMessage;

    public CauseRequest getCause() {
        return cause;
    }

    public void setCause(CauseRequest cause) {
        this.cause = cause;
    }

    public List<StackTraceRequest> getStackTrace() {
        return stackTrace;
    }

    public void setStackTrace(List<StackTraceRequest> stackTrace) {
        this.stackTrace = stackTrace;
    }

    public List<SuppressedException> getSuppressedExceptions() {
        return suppressedExceptions;
    }

    public void setSuppressedExceptions(List<SuppressedException> suppressedExceptions) {
        this.suppressedExceptions = suppressedExceptions;
    }

    public String getDetailMessage() {
        return detailMessage;
    }

    public void setDetailMessage(String detailMessage) {
        this.detailMessage = detailMessage;
    }

}