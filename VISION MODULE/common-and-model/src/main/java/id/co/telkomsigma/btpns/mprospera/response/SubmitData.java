package id.co.telkomsigma.btpns.mprospera.response;

public class SubmitData {

    private String transmissionDateAndTime;
    private String retrievalReferenceNumber;
    private String username;
    private String sessionKey;
    private String imei;
    private String processor;
    private String ram;
    private String gpsVersion;
    private String totalDeviceMemory;
    private String operatorName;
    private String signal;
    private String freeDeviceMemory;
    private String errorLog;
    private String tokenKey;

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public String getSignal() {
        return signal;
    }

    public void setSignal(String signal) {
        this.signal = signal;
    }

    public String getTransmissionDateAndTime() {
        return transmissionDateAndTime;
    }

    public void setTransmissionDateAndTime(String transmissionDateAndTime) {
        this.transmissionDateAndTime = transmissionDateAndTime;
    }

    public String getRetrievalReferenceNumber() {
        return retrievalReferenceNumber;
    }

    public void setRetrievalReferenceNumber(String retrievalReferenceNumber) {
        this.retrievalReferenceNumber = retrievalReferenceNumber;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getProcessor() {
        return processor;
    }

    public void setProcessor(String processor) {
        this.processor = processor;
    }

    public String getRam() {
        return ram;
    }

    public void setRam(String ram) {
        this.ram = ram;
    }

    public String getGpsVersion() {
        return gpsVersion;
    }

    public void setGpsVersion(String gpsVersion) {
        this.gpsVersion = gpsVersion;
    }

    public String getTotalDeviceMemory() {
        return totalDeviceMemory;
    }

    public void setTotalDeviceMemory(String totalDeviceMemory) {
        this.totalDeviceMemory = totalDeviceMemory;
    }

    public String getFreeDeviceMemory() {
        return freeDeviceMemory;
    }

    public void setFreeDeviceMemory(String freeDeviceMemory) {
        this.freeDeviceMemory = freeDeviceMemory;
    }

    public String getErrorLog() {
        return errorLog;
    }

    public void setErrorLog(String errorLog) {
        this.errorLog = errorLog;
    }

    public String getTokenKey() {
        return tokenKey;
    }

    public void setTokenKey(String tokenKey) {
        this.tokenKey = tokenKey;
    }

    @Override
    public String toString() {
        return "SubmitData{" +
                "transmissionDateAndTime='" + transmissionDateAndTime + '\'' +
                ", retrievalReferenceNumber='" + retrievalReferenceNumber + '\'' +
                ", username='" + username + '\'' +
                ", sessionKey='" + sessionKey + '\'' +
                ", imei='" + imei + '\'' +
                ", processor='" + processor + '\'' +
                ", ram='" + ram + '\'' +
                ", gpsVersion='" + gpsVersion + '\'' +
                ", totalDeviceMemory='" + totalDeviceMemory + '\'' +
                ", operatorName='" + operatorName + '\'' +
                ", signal='" + signal + '\'' +
                ", freeDeviceMemory='" + freeDeviceMemory + '\'' +
                ", errorLog='" + errorLog + '\'' +
                ", tokenKey='" + tokenKey + '\'' +
                '}';
    }

}