package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.manager.SentraManager;
import id.co.telkomsigma.btpns.mprospera.model.location.Location;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.model.wisma.ScoringMmsPiloting;
import id.co.telkomsigma.btpns.mprospera.request.LoginRequest;
import id.co.telkomsigma.btpns.mprospera.request.ProsperaLoginRequest;
import id.co.telkomsigma.btpns.mprospera.response.LoginResponse;
import id.co.telkomsigma.btpns.mprospera.response.ProsperaLoginResponse;
import id.co.telkomsigma.btpns.mprospera.service.*;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.math.BigDecimal;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.*;

//LOGIN API M-PROSPERA ANDROID
@SuppressWarnings("ALL")
@Controller
public class LoginTerminalController extends GenericController {

    private static final String PILOTING = "Piloting";
    private static final String REGULER = "Reguler";

    @Value("${terminal.sessionTimeout}")
    private String terminalTimeout;

    @Autowired
    private TerminalService terminalService;

    @Autowired
    private AreaService areaService;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private UserService userService;

    @Autowired
    private SentraManager sentraManager;

    @Autowired
    private ProsperaEncryptionService prosperaEncryptionService;

    @Autowired
    private WSValidationService wsValidationService;

    @Autowired
    private ParameterService paramService;

    @Autowired
    private RESTClient restClient;

    @Autowired
    private PilotingService pilotingService;

    @Value("${login.roaming:1}")
    private String loginRoaming;

    JsonUtils jsonUtils = new JsonUtils();

    @RequestMapping(value = WebGuiConstant.TERMINAL_LOGIN_PATH, method = {RequestMethod.POST})
    public @ResponseBody
    LoginResponse doLogin(@RequestBody LoginRequest request,
                          @PathVariable("apkVersion") String versionNumber) throws KeyManagementException, NoSuchAlgorithmException {

        try {
            log.info("INCOMING LOGIN MESSAGE FOR USER : " + jsonUtils.maskingPasswordJson(jsonUtils.toJson(request)));
        } catch (Exception e2) {
        }

        LoginResponse response = new LoginResponse();
        // username, imei, apk version validation
        log.info("Try to validating request");
        /*------------------------------------------------------*/
        // initiate terminal
        Terminal terminal = new Terminal();
        // initiate messageLogsList
        List<MessageLogs> messageLogsList = new ArrayList<>();
        /*------------------------------------------------------*/
        try {
            String validation = wsValidationService.loginValidation(request.getUsername(), request.getImei(),
                    versionNumber);
            response.setRetrievalReferenceNumber(request.getRetrievalReferenceNumber());
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                log.error(
                        "Validation Failed for username : " + request.getUsername() + " ,imei : " + request.getImei());
                if (validation.equals(WebGuiConstant.RC_INVALID_APK)) {
                    // Send link download new APK to device
                    response.setDownloadUrl(
                            paramService.loadParamByParamName(WebGuiConstant.PARAMETER_DOWNLOAD_LINK, ""));
                }
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.info("RESPONSE MESSAGE : " + jsonUtils.toJson(response));
                return response;
            }
            log.info("Check Terminal");
            // Create terminal
            if (terminalService.loadTerminalByImei(request.getImei().trim()) == null) {
                log.info("TERMINAL NOT FOUND, CREATING...");
                terminal.setAndroidBasebandVersion(request.getAndroidBasebandVersion());
                terminal.setAndroidBuildNumber(request.getAndroidBuildNumber());
                terminal.setAndroidKernelVersion("");
                terminal.setAndroidVersion(request.getAndroidVersion());
                terminal.setApplicationVersion(versionNumber);
                terminal.setCreatedDate(new Date());
                terminal.setFreeDeviceMemory(request.getFreeDeviceMemory());
                terminal.setGpsVersion(request.getGpsVersion());
                terminal.setImei(request.getImei());
                terminal.setModelNumber(request.getModelNumber());
                terminal.setProcessor(request.getProcessor());
                terminal.setRam(request.getRam());
                terminal.setTotalDeviceMemory(request.getTotalDeviceMemory());
                terminal.setLogin(false);
                log.info("Try to create new terminal");
                terminalService.createTerminal(terminal);
                log.info("Success to create new terminal");
            } else {
                log.info("TERMINAL FOUND");
                terminal = terminalService.loadTerminalByImei(request.getImei().trim());
            }
            log.info("Compose message for ESB");
            ProsperaLoginRequest prosperaLogin = new ProsperaLoginRequest();
            prosperaLogin.setUsername(request.getUsername().trim());
            // Encrypt Password
            try {
                prosperaLogin.setPassword(prosperaEncryptionService.encryptField(request.getPassword().trim()));
            } catch (Exception e1) {
                log.error("PASSWORD ENCRYPTION ERROR..., " + e1.getMessage());
            }
            prosperaLogin.setImei(request.getImei().trim());
            prosperaLogin.setTransmissionDateAndTime(request.getTransmissionDateAndTime().trim());
            prosperaLogin.setRetrievalReferenceNumber(request.getRetrievalReferenceNumber().trim());
            // Logging to messages logs incoming messages from android
            String districtCode = null;
            // access url prospera via ESB and get the response
            log.info("Try to send login request to Prospera via ESB");
            ProsperaLoginResponse prosperaLoginResponse = restClient.login(prosperaLogin);
            log.info("Get Response from ESB : " + prosperaLoginResponse.getResponseCode());
            response.setResponseCode(prosperaLoginResponse.getResponseCode());
            response.setResponseMessage(prosperaLoginResponse.getResponseMessage());
            if (prosperaLoginResponse.getResponseCode().equals(WebGuiConstant.RC_SUCCESS)) {
                response.setOfficeId(prosperaLoginResponse.getOfficeId());
                response.setState(prosperaLoginResponse.getState());
                // Mapping Role User
                List<String> roles = new ArrayList<>();
                for (String role : Arrays.asList(prosperaLoginResponse.getRole())) {
                    log.info("ROLE FROM PROSPERA : " + role);
                    log.debug("ROLE PS FROM PARAM : " + paramService
                            .loadParamByParamName(WebGuiConstant.PARAMETER_ROLE_USER, "Petugas Sentra Online"));
                    log.debug("ROLE MS FROM PARAM : " + paramService
                            .loadParamByParamName(WebGuiConstant.PARAMETER_ROLE_OTORISASI, "Manager Sentra Online"));
                    log.debug("ROLE NON MS/PS FROM PARAM : "
                            + paramService.loadParamByParamName(WebGuiConstant.PARAMETER_ROLE_NON_MS, "PM"));
                    if (paramService.loadParamByParamName(WebGuiConstant.PARAMETER_ROLE_USER, "Petugas Sentra Online")
                            .contains(role)) {
                        roles.add("1");
                        log.info("ROLE MAPPING VALUE : 1");
                    } else if (paramService
                            .loadParamByParamName(WebGuiConstant.PARAMETER_ROLE_OTORISASI, "Manager Sentra Online")
                            .contains(role)) {
                        roles.add("2");
                        log.info("ROLE MAPPING VALUE : 2");
                    } else if (paramService.loadParamByParamName(WebGuiConstant.PARAMETER_ROLE_NON_MS, "PM")
                            .contains(role)) {
                        roles.add("3");
                        log.info("ROLE MAPPING VALUE : 3");
                    }
                }
                if (roles.isEmpty()) {
                    response.setResponseCode(WebGuiConstant.RC_ROLE_NULL);
                    String label = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(label);
                    log.error("PARAMATER ROLE BELUM DIINPUT");
                    log.info("RESPONSE MESSAGE : " + response.toString());
                    return response;
                }
                response.setRole(roles.toArray(new String[0]));
                List<String> centers = new ArrayList<>();
                List<Sentra> sentraList = new ArrayList<>();
                // Mapping User to Sentra
                log.info("Find Sentra");
                for (String sentra : Arrays.asList(prosperaLoginResponse.getCenters())) {
                    Sentra byProsperaId = sentraManager.findByProsperaId("" + sentra);
                    if (byProsperaId != null) {
                        centers.add("" + byProsperaId.getSentraId());
                        if (!request.getUsername().equals(byProsperaId.getAssignedTo())) {
                            byProsperaId.setAssignedTo(request.getUsername());
                            sentraList.add(byProsperaId);
                        }
                    }
                }
                if (sentraList.size() > 0) {
                    sentraManager.save(sentraList);
                }
                log.info("Updating Sentra Done");
                response.setCenters(centers.toArray(new String[0]));
                if (prosperaLoginResponse.getState() != null) {
                    String[] stateData = prosperaLoginResponse.getState().split("-");
                    if (stateData.length > 0) {
                        if (stateData[0] != null)
                            districtCode = stateData[0].trim();
                    }
                }
            }
            if (response.getResponseCode().equals("00")) {
                userService.addMobileUser(request.getUsername(), request.getImei(), prosperaLoginResponse.getState(),
                        prosperaLoginResponse.getOfficeId());
                // Update session key
                log.info("Get User to Set Session Key");
                User user = (User) userService.loadUserByUsername(request.getUsername());
                response.setUserId(user.getUserId().toString());
                response.setOfficeId(user.getOfficeCode());
                if (user.getOfficeCode() != null) {
                    Location wisma = areaService.findLocationById(user.getOfficeCode());
                    if (wisma != null) {
                        response.setOfficeCode(wisma.getLocationCode());
                        response.setOfficeName(wisma.getName());
                        //TODO add office status
                        ScoringMmsPiloting piloting = pilotingService.getActivePilotingOffice(wisma.getLocationCode());
                        if (piloting != null) {
                            response.setOfficeStatus(PILOTING);
                        } else {
                            response.setOfficeStatus(REGULER);
                        }
                        Location kfoLocation = areaService.findLocationById(wisma.getParentLocation());
                        if (kfoLocation != null) {
                            response.setKfoName(kfoLocation.getName());
                            response.setKfoCode(kfoLocation.getLocationCode());
                        }
                    }
                }
                if (response.getRole() != null)
                    user.setRoleUser(response.getRole()[0]);
                log.info("Success to get user : " + user.getUsername());
                if (user.getSessionKey() != null) {
                    if (!"1".equals(loginRoaming)) {
                        if (user.getLoginImei() != null) {
                            if (!user.getLoginImei().equals(request.getImei())) {
                                log.error("User sedang digunakan");
                                response.setResponseCode(WebGuiConstant.RC_USERNAME_USED);
                                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                                response.setResponseMessage(label);
                                log.info("RESPONSE MESSAGE : " + jsonUtils.toJson(response));
                                return response;
                            }
                        } else {
                            log.error("User sedang digunakan");
                            response.setResponseCode(WebGuiConstant.RC_USERNAME_USED);
                            String label = getMessage("webservice.rc.label." + response.getResponseCode());
                            response.setResponseMessage(label);
                            log.info("RESPONSE MESSAGE : " + jsonUtils.toJson(response));
                            return response;
                        }
                    }
                }
                // set limit BWMP User Approval
                if (user.getLimit() != null) {
                    if (!user.getLimit().equals("")) {
                        if (new BigDecimal(user.getLimit()).compareTo(BigDecimal.ZERO) != 0) {
                            response.setLimitBwmp(user.getLimit());
                        } else {
                            response.setLimitBwmp(
                                    paramService.loadParamByParamName(WebGuiConstant.PARAMETER_LIMIT_BWMP, "0"));
                        }
                    } else {
                        response.setLimitBwmp(
                                paramService.loadParamByParamName(WebGuiConstant.PARAMETER_LIMIT_BWMP, "0"));
                    }
                } else {
                    response.setLimitBwmp(paramService.loadParamByParamName(WebGuiConstant.PARAMETER_LIMIT_BWMP, "0"));
                }
                user.setLoginImei(request.getImei());
                user.setSessionKey(UUID.randomUUID().toString());
                terminal.setLogin(true);
                user.setSessionTime(terminalTimeout);
                user.setSessionCreatedDate(new Date());
                user.setDistrictCode(districtCode);
                log.info("Updating terminal");
                terminalService.updateTerminal(terminal);
                log.info("Updating terminal success");
                log.info("Updating user");
                userService.updateUser(user);
                log.info("Updating user success");
                // set session key
                response.setSessionKey(user.getSessionKey());
                response.setTokenKey("");
            }
        } catch (Exception e) {
            log.error("FETCHING TERMINAL_LOGIN_PATH FAILED....." + e.getMessage());
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            String label = getMessage("webservice.rc.label." + response.getResponseCode());
            response.setResponseMessage(label);
        } finally {
            try {

                log.info("Trying to Create Terminal Activity...");
                // create terminal activity record
                TerminalActivity terminalActivity = new TerminalActivity();
                // Logging to terminal activity
                terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                terminalActivity.setActivityType(TerminalActivity.ACTIVITY_MOBILE_LOGIN);
                terminalActivity.setCreatedDate(new Date());
                terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                terminalActivity.setTerminal(terminal);
                terminalActivity.setUsername(request.getUsername().trim());
                log.info("TERMINAL ACTIVITY LOGGING SUCCESS...");
                log.info("Updating Terminal Activity");
                terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogsList);
            } catch (Exception e) {
                log.error("ERROR...CREATE logging Terminal Activity and Message Logs" + e.getMessage());
            }
        }
        log.info("RESPONSE MESSAGE LOG MPROS to ANDRO LOGGING SUCCESS...");
        log.info("Send Response to Handset");
        try {
            log.info("RESPONSE MESSAGE : " + new JsonUtils().toJson(response));
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        return response;
    }

}