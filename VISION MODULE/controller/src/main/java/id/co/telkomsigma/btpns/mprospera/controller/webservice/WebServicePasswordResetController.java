package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.request.ESBPasswordResetRequest;
import id.co.telkomsigma.btpns.mprospera.request.PasswordResetRequest;
import id.co.telkomsigma.btpns.mprospera.response.BaseResponse;
import id.co.telkomsigma.btpns.mprospera.service.ProsperaEncryptionService;
import id.co.telkomsigma.btpns.mprospera.service.TerminalActivityService;
import id.co.telkomsigma.btpns.mprospera.service.TerminalService;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.websocket.server.PathParam;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//RESET PASSWORD USER LOGIN M-PROSPERA API
@Controller
public class WebServicePasswordResetController extends GenericController {

    @Autowired
    private TerminalService terminalService;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private ProsperaEncryptionService prosperaEncryptionService;

    @Autowired
    private RESTClient restClient;

    final JsonUtils jsonUtils = new JsonUtils();

    private static final Logger LOGGER = LoggerFactory.getLogger(WebServicePasswordResetController.class);

    @RequestMapping(value = WebGuiConstant.PASSWORD_RESET_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    BaseResponse doReset(@RequestBody PasswordResetRequest request,
                         @PathParam("apkVersion") String versionNumber) throws IOException {

        log.info("INCOMING RESET PASSWORD MESSAGE : " + jsonUtils.toJson(request));
        BaseResponse response = new BaseResponse();
        Terminal terminal;
        try {
            // Create terminal
            if (terminalService.loadTerminalByImei(request.getImei().trim()) == null) {
                log.error("TERMINAL NOT FOUND...");
                response.setResponseCode(WebGuiConstant.RC_TERMINAL_NOT_FOUND);
                response.setResponseMessage(getMessage("webservice.rc.label." + response.getResponseCode()));
                log.info("RESPONSE MESSAGE : " + jsonUtils.toJson(response));
                return response;
            } else {
                log.info("TERMINAL FOUND");
                terminal = terminalService.loadTerminalByImei(request.getImei().trim());
            }
            log.info("Trying to CREATE Terminal Activity....");
            // create terminal activity record
            TerminalActivity terminalActivity = new TerminalActivity();
            // Logging to terminal activity
            terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
            terminalActivity.setActivityType(TerminalActivity.ACTIVITY_PASS_RESET);
            terminalActivity.setCreatedDate(new Date());
            terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
            if (request.getSessionKey() != null)
                terminalActivity.setSessionKey(request.getSessionKey());
            terminalActivity.setTerminal(terminal);
            terminalActivity.setUsername(request.getUsername().trim());
            log.info("TERMINAL ACTIVITY LOGGING SUCCESS...");
            // Logging to message logs incoming message from android
            List<MessageLogs> messageLogsList = new ArrayList<>();
            MessageLogs messageLogs = new MessageLogs();
            messageLogs.setCreatedDate(new Date());
            messageLogs.setEndpointCode("MPROSPERA");
            messageLogs.setRequest(true);
            messageLogs.setTerminalActivity(terminalActivity);
            try {
                messageLogs.setMessageRaw(new JsonUtils().toJson(request).trim().getBytes());
            } catch (JsonMappingException e) {
                log.error(e.getMessage(), e);
            } catch (JsonGenerationException e) {
                log.error(e.getMessage(), e);
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
            messageLogsList.add(messageLogs);
            log.info("REQUEST MESSAGE LOG ANDRO to MPROS LOGGING SUCCESS...");
            log.info("Send request reset password to Prospera via ESB");
            ESBPasswordResetRequest esbRequest = new ESBPasswordResetRequest();
            esbRequest.setUsername(request.getUsername().trim());
            esbRequest.setImei(request.getImei().trim());
            esbRequest.setTransmissionDateAndTime(request.getTransmissionDateAndTime().trim());
            esbRequest.setRetrievalReferenceNumber(request.getRetrievalReferenceNumber().trim());
            esbRequest.setTokenChallenge(request.getTokenChallenge());
            esbRequest.setTokenResponse(request.getTokenResponse());
            try {
                esbRequest.setOldPassword(prosperaEncryptionService.encryptField(request.getOldPassword().trim()));
                esbRequest.setNewPassword(prosperaEncryptionService.encryptField(request.getNewPassword().trim()));
            } catch (Exception e1) {
                LOGGER.error("PASSWORD ENCRYPTION ERROR..., " + e1.getMessage());
                response.setResponseCode("XX");
                response.setResponseMessage("PASSWORD ENCRYPTION ERROR");
                log.info("RESPONSE MESSAGE : " + jsonUtils.toJson(response));
                return response;
            }
            // Logging to message logs sent message to ESB
            messageLogs = new MessageLogs();
            messageLogs.setCreatedDate(new Date());
            messageLogs.setEndpointCode("ESB");
            messageLogs.setRequest(true);
            messageLogs.setTerminalActivity(terminalActivity);
            try {
                messageLogs.setMessageRaw(new JsonUtils().toJson(esbRequest).getBytes());
            } catch (JsonMappingException | JsonGenerationException e) {
                log.error(e.getMessage(), e);
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
            messageLogsList.add(messageLogs);
            log.info("REQUEST MESSAGE LOG MPROS to ESB LOGGING SUCCESS...");
            // access url prospera via ESB and get the response
            BaseResponse esbResponse = restClient.passwordReset(esbRequest);
            response.setResponseCode(esbResponse.getResponseCode());
            response.setResponseMessage(esbResponse.getResponseMessage());
            // Logging to message logs incoming message from ESB
            messageLogs = new MessageLogs();
            messageLogs.setCreatedDate(new Date());
            messageLogs.setEndpointCode("ESB");
            messageLogs.setRequest(false);
            messageLogs.setTerminalActivity(terminalActivity);
            try {
                messageLogs.setMessageRaw(new JsonUtils().toJson(esbResponse).getBytes());
            } catch (JsonMappingException | JsonGenerationException e) {
                log.error(e.getMessage(), e);
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
            messageLogsList.add(messageLogs);
            log.info("RESPONSE MESSAGE LOG ESB to MPROS LOGGING SUCCESS...");
            // Logging to message logs incoming message from ESB
            messageLogs = new MessageLogs();
            messageLogs.setCreatedDate(new Date());
            messageLogs.setEndpointCode("MPROSPERA");
            messageLogs.setRequest(false);
            messageLogs.setTerminalActivity(terminalActivity);
            try {
                messageLogs.setMessageRaw(new JsonUtils().toJson(response).getBytes());
            } catch (JsonMappingException | JsonGenerationException e) {
                log.error(e.getMessage(), e);
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
            messageLogsList.add(messageLogs);
            terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogsList);
            log.info("RESPONSE MESSAGE LOG MPROS to ANDRO LOGGING SUCCESS...");
        } catch (Exception e) {
            log.error("ERROR on PASSWORD_RESET_REQUEST : " + e.getMessage(), e);
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            String labelError = getMessage("webservice.rc.label." + response.getResponseCode());
            response.setResponseCode(labelError);
        }
        log.info("RESPONSE MESSAGE : " + jsonUtils.toJson(response));
        return response;
    }

}