package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.request.LogoutRequest;
import id.co.telkomsigma.btpns.mprospera.response.BaseResponse;
import id.co.telkomsigma.btpns.mprospera.service.*;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.websocket.server.PathParam;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

//LOGOUT API ANDROID M-PROSPERA
@Controller
public class LogoutTerminalController extends GenericController {

    protected final Log LOGGER = LogFactory.getLog(getClass());

    @Autowired
    private TerminalService terminalService;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private UserService userService;

    final JsonUtils jsonUtils = new JsonUtils();

    @RequestMapping(value = WebGuiConstant.TERMINAL_LOGOUT_PATH, method = {RequestMethod.POST})
    public @ResponseBody
    BaseResponse doLogout(@RequestBody LogoutRequest request,
                          @PathParam("apkVersion") String versionNumber) throws IOException {

        log.info("INCOMING LOGOUT MESSAGE : " + jsonUtils.toJson(request));
        BaseResponse response = new BaseResponse();
        // Check terminal
        LOGGER.info("Trying to FIND Terminal ");
        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
        User user = (User) userService.loadUserByUsername(request.getUsername());
        try {
            if (terminal == null) {
                LOGGER.error("TERMINAL NOT FOUND");
                response.setResponseCode(WebGuiConstant.RC_TERMINAL_NOT_FOUND);
                response.setResponseMessage(getMessage("webservice.rc.label." + response.getResponseCode()));
                log.error("RESPONSE MESSAGE : " + jsonUtils.toJson(response));
                return response;
            }
            if (user == null) {
                LOGGER.error("USER NOT FOUND");
                response.setResponseCode(WebGuiConstant.RC_USERNAME_NOT_EXISTS);
                response.setResponseMessage(getMessage("webservice.rc.label." + response.getResponseCode()));
                log.error("RESPONSE MESSAGE : " + jsonUtils.toJson(response));
                return response;
            }
            // logout processing
            terminalService.logout(terminal);
            userService.logout(user);
            response.setResponseCode("00");
            response.setResponseMessage("SUCCESS");
            LOGGER.info("SESSION KEY RESET PROCESS SUCCESS...");
        } catch (Exception e) {
            response.setResponseCode("XX");
            LOGGER.error("SESSION KEY RESETTING PROCESS FAILED..., " + e.getMessage());
            String labelError = getMessage("webservice.rc.label." + response.getResponseCode());
            response.setResponseMessage(labelError);
        } finally {
            try {
                LOGGER.trace("Trying to CREATE Terminal Activity Record......");
                // create terminal activity record
                TerminalActivity terminalActivity = new TerminalActivity();
                // Logging to terminal activity
                terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                terminalActivity.setActivityType(TerminalActivity.ACTIVITY_MOBILE_LOGOUT);
                terminalActivity.setCreatedDate(new Date());
                terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                terminalActivity.setSessionKey(request.getSessionKey().trim());
                terminalActivity.setTerminal(terminal);
                terminalActivity.setUsername(request.getUsername().trim());
                log.info("TERMINAL ACTIVITY LOGGING SUCCESS...");
                // Logging to messages logs incoming messages from android
                List<MessageLogs> messageLogsList = new ArrayList<>();
                log.info("Trying to SAVE Terminal Activity Record......");
                terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogsList);
                log.info("RESPONSE MESSAGE LOG MPROS to ANDRO LOGGING SUCCESS...");
            } catch (Exception e) {
                LOGGER.error("FAILED.... to CREATE or SAVE Terminal Activity Record......" + e.getMessage());
            }
        }
        log.info("RESPONSE MESSAGE : " + jsonUtils.toJson(response));
        return response;
    }

}