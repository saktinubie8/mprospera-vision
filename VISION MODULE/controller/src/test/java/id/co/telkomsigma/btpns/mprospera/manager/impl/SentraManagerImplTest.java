package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.SentraDao;
import id.co.telkomsigma.btpns.mprospera.manager.SentraManager;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class SentraManagerImplTest {

    @TestConfiguration
    static class SentraManagerImplTestContextConfiguration {
        @Bean
        public SentraManager sentraManager() {
            return new SentraManagerImpl();
        }
    }

    @Autowired
    private SentraManager sentraManager;

    @MockBean
    private SentraDao sentraDao;

    @Before
    public void setUp() {
        Sentra sentra = new Sentra();
        sentra.setProsperaId("1011665");
        Mockito.when(sentraDao.findByProsperaId(sentra.getProsperaId()))
                .thenReturn(sentra);
        Mockito.when(sentraDao.save(sentra)).thenReturn(sentra);
    }

    @Test
    public void WhenGetByProsperaId_ThenSentraShouldBeFound() {
        Sentra newSentra = new Sentra();
        newSentra.setProsperaId("1011665");
        Sentra found = sentraManager.findByProsperaId(newSentra.getProsperaId());
        assertThat(found.getProsperaId()).isEqualTo(newSentra.getProsperaId());
    }

    @Test
    public void WhenSaveSentra() {
        Sentra newSentra = new Sentra();
        newSentra.setSentraName("SENTRA BARU");
        sentraManager.save(newSentra);
        assertEquals(newSentra.getSentraName(), newSentra.getSentraName());
    }

}