package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.dao.ParamDao;
import id.co.telkomsigma.btpns.mprospera.manager.ParamManager;
import id.co.telkomsigma.btpns.mprospera.manager.impl.ParamManagerImpl;
import id.co.telkomsigma.btpns.mprospera.model.parameter.SystemParameter;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class ParameterServiceTest {

    @TestConfiguration
    static class ParameterServiceTestContextConfiguration {
        @Bean
        public ParameterService parameterService() {
            return new ParameterService();
        }

        @Bean
        public ParamManager paramManager() {
            return new ParamManagerImpl();
        }
    }

    @Autowired
    private ParameterService parameterService;

    @MockBean
    private ParamDao paramDao;

    @Before
    public void setUp() {
        SystemParameter parameter = new SystemParameter();
        parameter.setParamName("android.apk.url.download");
        parameter.setParamValue("http://202.158.9.131:8889/mprospera/apk/get-apk?version=3.69");
        User user = new User();
        parameter.setCreatedBy(user);
        Mockito.when(paramDao.findByParamName(parameter.getParamName()))
                .thenReturn(parameter);
    }

    @Test
    public void whenGetByParamName_thenParamShouldBeFound() {
        String paramName = "android.apk.url.download";
        String paramValue = "http://202.158.9.131:8889/mprospera/apk/get-apk?version=3.69";
        String found = parameterService.loadParamByParamName(paramName, "");
        assertThat(found).isEqualTo(paramValue);
    }

}