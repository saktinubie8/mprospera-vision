package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.dao.ManageApkDao;
import id.co.telkomsigma.btpns.mprospera.dao.TerminalActivityDao;
import id.co.telkomsigma.btpns.mprospera.dao.TerminalDao;
import id.co.telkomsigma.btpns.mprospera.dao.UserDao;
import id.co.telkomsigma.btpns.mprospera.manager.ManageApkManager;
import id.co.telkomsigma.btpns.mprospera.manager.TerminalManager;
import id.co.telkomsigma.btpns.mprospera.manager.UserManager;
import id.co.telkomsigma.btpns.mprospera.manager.impl.ManageApkManagerImpl;
import id.co.telkomsigma.btpns.mprospera.manager.impl.TerminalManagerImpl;
import id.co.telkomsigma.btpns.mprospera.manager.impl.UserManagerImpl;
import id.co.telkomsigma.btpns.mprospera.model.manageApk.ManageApk;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class WSValidationServiceTest {

    @TestConfiguration
    static class WSValidationServiceTestContextConfiguration {
        @Bean
        public WSValidationService wsValidationService() {
            return new WSValidationService();
        }

        @Bean
        public ManageApkManager manageApkManager() {
            return new ManageApkManagerImpl();
        }

        @Bean
        public TerminalManager terminalManager() {
            return new TerminalManagerImpl();
        }

        @Bean
        public UserManager userManager() {
            return new UserManagerImpl();
        }

        @Bean
        public PasswordEncoder passwordEncoder() {
            return new PasswordEncoder() {
                @Override
                public String encode(CharSequence charSequence) {
                    return null;
                }

                @Override
                public boolean matches(CharSequence charSequence, String s) {
                    return false;
                }
            };
        }
    }

    @Autowired
    private WSValidationService wsValidationService;

    @MockBean
    private ManageApkDao manageApkDao;

    @MockBean
    private TerminalDao terminalDao;

    @MockBean
    private TerminalActivityDao terminalActivityDao;

    @MockBean
    private UserDao userDao;

    @Before
    public void setUp() {
        ManageApk currentApkVersion = new ManageApk();
        currentApkVersion.setApkVersion("3.69");
        currentApkVersion.setAllowedVersion(true);
        Mockito.when(manageApkDao.findByApkVersion(currentApkVersion.getApkVersion())).
                thenReturn(currentApkVersion);
    }

    @Test
    public void whenValidRequest_thenValidationIsTrue() {
        String apkVersion = "3.69";
        String username = "w0211v";
        String imei = "358525070497940";
        String result = wsValidationService.loginValidation(username, imei, apkVersion);
        assertThat(result).isEqualTo("00");
    }

}