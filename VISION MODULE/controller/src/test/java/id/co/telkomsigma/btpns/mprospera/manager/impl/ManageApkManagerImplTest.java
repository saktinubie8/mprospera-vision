package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.ManageApkDao;
import id.co.telkomsigma.btpns.mprospera.manager.ManageApkManager;
import id.co.telkomsigma.btpns.mprospera.model.manageApk.ManageApk;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class ManageApkManagerImplTest {

    @TestConfiguration
    static class ManageApkManagerImplTestContextConfiguration {
        @Bean
        public ManageApkManager manageApkManager() {
            return new ManageApkManagerImpl();
        }
    }

    @Autowired
    private ManageApkManager manageApkManager;

    @MockBean
    private ManageApkDao manageApkDao;

    @Before
    public void setUp() {
        ManageApk currentApkVersion = new ManageApk();
        currentApkVersion.setApkVersion("3.69");
        Mockito.when(manageApkDao.findByApkVersion(currentApkVersion.getApkVersion())).
                thenReturn(currentApkVersion);
    }

    @Test
    public void whenValidApk_thenApkShouldBeFound() {
        String apkVersion = "3.69";
        ManageApk found = manageApkManager.getByVersion(apkVersion);
        assertThat(found.getApkVersion()).isEqualTo(apkVersion);
    }

}