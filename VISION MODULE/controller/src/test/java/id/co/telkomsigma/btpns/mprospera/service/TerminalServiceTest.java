package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.dao.TerminalActivityDao;
import id.co.telkomsigma.btpns.mprospera.dao.TerminalDao;
import id.co.telkomsigma.btpns.mprospera.manager.TerminalManager;
import id.co.telkomsigma.btpns.mprospera.manager.impl.TerminalManagerImpl;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class TerminalServiceTest {

    @TestConfiguration
    static class TerminalServiceTestContextConfiguration {
        @Bean
        public TerminalService terminalService() {
            return new TerminalService();
        }

        @Bean
        public TerminalManager terminalManager() {
            return new TerminalManagerImpl();
        }
    }

    @Autowired
    private TerminalService terminalService;

    @MockBean
    private TerminalDao terminalDao;

    @MockBean
    TerminalActivityDao terminalActivityDao;

    @Before
    public void setUp() {
        Terminal terminal = new Terminal();
        terminal.setImei("358525070497940");
        terminal.setAndroidVersion("19");
        terminal.setCreatedDate(new Date());
        Mockito.when(terminalDao.findByImei(terminal.getImei()))
                .thenReturn(terminal);
        Mockito.when(terminalDao.save(terminal)).thenReturn(terminal);
    }

    @Test
    public void WhenGetByImei_ThenTerminalShouldBeFound() {
        String imei = "358525070497940";
        Terminal found = terminalService.loadTerminalByImei(imei);
        assertThat(found.getImei()).isEqualTo(imei);
    }

    @Test
    public void WhenSaveTerminal() {
        Terminal newTerminal = new Terminal();
        newTerminal.setImei("358525070497940");
        newTerminal.setAndroidVersion("19");
        newTerminal.setCreatedDate(new Date());
        Terminal result = terminalService.createTerminal(newTerminal);
        System.out.println(result);
        assertEquals(result.getImei(), "358525070497940");
        assertEquals(result.getAndroidVersion(), "19");
    }

}